import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import Components from 'unplugin-vue-components/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import setupConfig from './config'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'
import viteCompression from 'vite-plugin-compression'
import AutoImport from 'unplugin-auto-import/vite'

export default ({ mode }: { mode: any }) => {
  const { build } = setupConfig({ mode })
  const plugins = []
  if (mode === 'lib') {
    plugins.push(cssInjectedByJsPlugin())
  } else {
    plugins.push(
      viteCompression({
        threshold: 10240
      })
    )
  }
  return defineConfig({
    root: path.resolve(__dirname, 'src'), // 修改root参数为多页面的根目录
    base: './',
    plugins: [
      vue(),
      Components({
        resolvers: [NaiveUiResolver()]
      }),
      AutoImport({
        imports: ['vue', 'vue-router', 'pinia'],
        dts: './auto-import.d.ts'
      }),
      ...plugins
    ],
    publicDir: 'public',
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        __ROOT__: path.resolve(__dirname, '')
      },
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'] // 自动匹配文件后缀名
    },
    build
  })
}
