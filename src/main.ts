import { createApp } from 'vue'
import 'animate.css'
import '@/style/reset.less'
import App, { init, router } from '@/App.vue'
// 页面布局
import PageLayout from '@/view/components/page-layout/index.vue'
// 分页组件
import Pagination from '@/view/components/pagination/index.vue'

const app = createApp(App)

// 全局组件挂载
app.component('PageLayout', PageLayout)
app.component('Pagination', Pagination)

app.use(init).use(router)
router.isReady().then(() => {
  app.mount('#app')
})
