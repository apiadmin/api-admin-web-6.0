import logo from "@/assets/logo-min.png"

export default {
    baseUrl: "https://api.****.com/admin/",
    whiteList: ["/login"],
    resetPath: "/login",
    website: {
        logo,
        title: "ApiAdmin"
    }
}
