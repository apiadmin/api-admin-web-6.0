import {post, get} from "@/http/request"

export const login = (params: any) => post("Login/index", params, {hint: true})

export const userInfo = () => get("Login/getUserInfo")

export const getCaptcha = () => get("Captcha/create")
