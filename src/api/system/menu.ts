import {get, post} from "@/http/request"

export const getList = (params: any) => get("Menu/index", params)

export const del = (id: any) => get("Menu/del", {id})

export const edit = (data: any) => post("Menu/edit", data)

export const add = (data: any) => post("Menu/add", data)

