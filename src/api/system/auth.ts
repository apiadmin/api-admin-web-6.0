import { get, post } from '@/http/request'

// 获取列表
export const getGroupList = (params: any) => get('Auth/index', params)

// 新增/编辑组
export const configGroup = (params: any) => post(`Auth/${params.id ? 'edit' : 'add'}`, params)

// 删除分组
export const delGroup = (params: any) => get('Auth/del', params)

// 修改组状态
export const changeGroupStatus = (params: any) => get('Auth/changeStatus', params)

// 组成员列表
export const getGroupMemberList = (params: any) => get('User/getUsers', params)

// 删除组成员
export const delMember = (params: any) => get('Auth/delMember', params)

// 获取权限规则
export const getRuleList = (params: any) => get('Auth/getRuleList', params)

// 配置权限
export const editRule = (params: any) => post('Auth/editRule', params)
