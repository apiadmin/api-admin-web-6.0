import {get, post} from "@/http/request"

export const getUsers = (params: any) => get("User/getUsers", params)

export const getList = (params: any) => get("User/index", params)

export const getGroups = () => get("Auth/getGroups")

export const changeStatus = (status: number, id: number) => get("User/changeStatus", {status, id})

export const config = (params: any) => post(`User/${params.id ? 'edit' : 'add'}`, params)

export const del = (id: any) => get("User/del", {id})

export const own = (data: any) => post("User/own", data)

