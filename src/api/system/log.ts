import {get} from "@/http/request"

export const getList = (params: any) => get("Log/index", params)

export const del = (id: number) => get("Log/del", {id})

