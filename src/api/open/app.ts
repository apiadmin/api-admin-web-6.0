import { get, post } from '@/http/request'

export const getList = (params: any) => get("App/index", params)

export const del = (id: any) => get("App/del", {id})

export const config = (params: any) => post(`App/${params.id ? 'edit' : 'add'}`, params)

export const changeStatus = (status: number, id: number) => get("App/changeStatus", {status, id})

export const refreshAppSecretApi = () => get("App/refreshAppSecret")

export const getAppInfo = (id: any) => get("App/getAppInfo", {id})
