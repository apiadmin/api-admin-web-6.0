import { get, post } from '@/http/request'

// 获取列表
export const getInterfaceList = (params: any) => get('Interface/index', params)

// 新增/编辑组
export const configInterface = (params: any) => post(`Interface/${params.id ? 'edit' : 'add'}`, params)

export const delInterface = (hash: any) => get('Interface/del', {hash})

export const changeInterfaceStatus = (status: number, hash: string) => get('Interface/changeStatus', {status, hash})

export const getHash = () => get('Interface/getHash')
