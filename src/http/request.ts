import axios, {AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig} from "axios"
import axiosRetry from "axios-retry"
import localStore from "@/utils/localStore"
import appPinia from "@/pinia/app"

const http: AxiosInstance = axios.create()

axiosRetry(http, {
    retries: 3,
    shouldResetTimeout: true,
    retryDelay: (retryCount) => retryCount * 1500 // 间隔时间
})

http.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    const appStore = appPinia()
    config.headers["Api-Auth"] = localStore.get("api_auth")
    config.baseURL = appStore.configOptions.baseUrl
    return config
}, (error: AxiosError) => {
    return Promise.reject(error)
})

http.interceptors.response.use((response: AxiosResponse) => {
    const {code, msg} = response.data
    if (code === 1) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (response.config.hint && msg) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            window.$message.success(msg)
        }
        return response.data
    }
    if (code < 0) {
        if(code === -14){
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            http.$router.push(http.$configOptions.resetPath)
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            window.$message.error(msg)
        }
    }

    return Promise.reject(response)
}, (error: AxiosError) => {
    return Promise.reject(error)
})

const post = (url: string, params?: any, config?: AxiosRequestConfig) => {
    return http.post(url, params, config)
}

const get = (url: string, params?: any, config?: AxiosRequestConfig) => {
    return http.get(url, {params, ...config})
}

export {
    post,
    get,
    http as axios,
}
