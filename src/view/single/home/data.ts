const timeline = [
    {
        "type": "success",
        "title": "Vite+Vue3.0+TypeScript",
        "content": "羊先生 初始化 Vite+Vue3.0+TypeScript 项目，定位目标适合企业中后台管理系统",
        "time": "2021-10-08 10:01:22"
    },
    {
        "type": "info",
        "title": "菜单问题",
        "content": "羊先生 修复了 菜单递归问题 已经打包上传",
        "time": "2021-11-27 22:01:22"
    },
    {
        "type": "warning",
        "title": "主题开发",
        "content": "羊先生 添加 多种主题切换",
        "time": "2021-12-6 18:01:22"
    },
    {
        "type": "warning",
        "title": "主题开发",
        "content": "羊先生 添加 多种主题切换",
        "time": "2021-12-6 18:01:22"
    },
]

export {
    timeline
}
