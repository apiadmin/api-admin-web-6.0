import {DataTableColumns} from "naive-ui"
import {editButton, delButton, statusElement} from "@/view/hooks/table-btn"

// 数据表格
export const createColumns = (compHandle: any): DataTableColumns => {
    return [
        {
            title: "ID",
            key: "id",
            align: "center",
            width: 80,
        },
        {
            title: "应用名称",
            key: "app_name",
            align: "center",
            minWidth: 130
        },
        {
            title: "AppId",
            key: "app_id",
            align: "center",
            width: 120
        },
        {
            title: "AppSecret",
            key: "app_secret",
            align: "center",
            width: 310
        },
        {
            title: "应用说明",
            key: "app_info",
            align: "center",
            width: 160,
            ellipsis: true
        },
        {
            title: "应用状态",
            key: "",
            align: "center",
            width: 120,
            render(row: any) {
                return statusElement(h, row, compHandle)
            }
        },
        {
            title: "操作",
            key: "actions",
            align: "center",
            width: 180,
            render(row: any) {
                return h("div", [
                    editButton(h, row, compHandle),
                    delButton(h, row, compHandle)
                ])
            }
        }
    ]
}


// 搜索及其分页
export const searchOptions = {
    status: [
        {
            label: "启用",
            value: 1
        },
        {
            label: "禁用",
            value: 0
        }
    ],
    type: [
        {
            label: "AppId",
            value: 1
        },
        {
            label: "应用名称",
            value: 2
        }
    ]
}

export const paginationReactive = reactive({
    page: 1,
    size: 10,
    total: 0
})

export const searchForm = reactive({
    type: null,
    keywords: "",
    status: null
})


// 表单提交
export const originFormItem = {
    app_name: '',
    app_id: '',
    app_secret: '',
    app_info: '',
    app_api: [],
    id: 0
}

export const formItem = reactive({
    ...originFormItem
})

export const formRules = {
    app_name: [
        { required: true, message: '应用名称不能为空', trigger: 'blur' }
    ]
}
