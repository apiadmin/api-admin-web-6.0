import {DataTableColumns, NTag} from "naive-ui"
import {editButton, delButton, statusElement} from "@/view/hooks/table-btn"

// 数据表格
export const createColumns = (compHandle: any): DataTableColumns => {
  return [
    {
      title: "ID",
      key: "id",
      align: "center",
      width: 80,
    },
    {
      title: "接口名称",
      key: "info",
      align: "center",
      minWidth: 190
    },
    {
      title: "真实类库",
      key: "api_class",
      align: "center",
      minWidth: 300
    },
    {
      title: "接口映射",
      key: "hash",
      align: "center",
      width: 160
    },
    {
      title: "请求方式",
      key: "",
      align: "center",
      width: 95,
      render(row: any) {
        return methodElement(h, row, compHandle)
      }
    },
    {
      title: "接口状态",
      key: "",
      align: "center",
      width: 120,
      render(row: any) {
        return statusElement(h, row, compHandle)
      }
    },
    {
      title: "操作",
      key: "actions",
      align: "center",
      width: 180,
      render(row: any) {
        return h("div", [
          editButton(h, row, compHandle),
          delButton(h, row, compHandle)
        ])
      }
    }
  ]
}

const methodElement = (h: any, row: any, compHandle: any) => {
  switch (row.method) {
    case 1:
      return h(NTag, {
        type: "success"
      }, { default: () => "POST" })
    case 2:
      return h(NTag, {
        type: "info"
      }, { default: () => "GET" })
    case 0:
      return h(NTag, {}, { default: () => "不限" })
  }
}


// 搜索及其分页
export const searchOptions = {
  status: [
    {
      label: "启用",
      value: 1
    },
    {
      label: "禁用",
      value: 0
    }
  ],
  type: [
    {
      label: "接口标识",
      value: 1
    },
    {
      label: "接口名称",
      value: 2
    },
    {
      label: "真实类库",
      value: 3
    }
  ]
}

export const paginationReactive = reactive({
  page: 1,
  size: 10,
  total: 0
})

export const searchForm = reactive({
  type: null,
  keywords: "",
  status: null
})


// 表单提交
export const formOptions = {
  method: [
    {
      label: "GET",
      value: 2
    },
    {
      label: "POST",
      value: 1
    },
    {
      label: "不限",
      value: 0
    }
  ],
  auth: [
    {
      label: "登录访问",
      value: 1
    },
    {
      label: "游客访问",
      value: 0
    }
  ],
  log: [
    {
      label: "记录日志",
      value: 1
    },
    {
      label: "不记录日志",
      value: 0
    }
  ],
  test: [
    {
      label: "测试模式",
      value: 1
    },
    {
      label: "生产模式",
      value: 0
    }
  ],
}

export const originFormItem = {
  api_class: '',
  info: '',
  limit: 0,
  method: 2,
  hash_type: 2,
  hash: '',
  access_token: 0,
  is_test: 0,
  auth: 1,
  log: 1,
  id: 0
}

export const formItem = reactive({
  ...originFormItem
})

export const formRules = {
  api_class: [
    {required: true, message: '真实类库不能为空', trigger: 'blur'}
  ],
  info: [
    {required: true, message: '接口名称不能为空', trigger: 'blur'}
  ]
}
